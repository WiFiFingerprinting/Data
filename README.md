WiFi SCans of 12 locations and a remtote set R.
Scans per second, containing access points in terms of their capability features. 

The data header of all individual APs consists of the following elements from [1]:

Capability Flags, WPA Flags, RSN Flags, Frequency, Mode, RSSI

[1] https://developer.gnome.org/NetworkManager/1.2/gdbus-org.freedesktop.NetworkManager.AccessPoint.html
